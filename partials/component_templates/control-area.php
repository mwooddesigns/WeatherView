<template id="control-area">
    <div class="controls">
        <select name="mode" v-model="localCurrentMode">
            <option v-for="mode in modes" :value="mode">{{ mode | capitalize }}</option>
        </select>
        <div class="row">
            <input type="text" v-model="localCityName" @keyup.enter="handleCitySubmit">
            <button>Update</button>
        </div>
    </div>
</template>