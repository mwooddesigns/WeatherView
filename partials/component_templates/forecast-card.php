<template id="forecast-card">
    <div class="forecast-card col-6">
        <div class="row break-sm">
            <div class="date">
                <h5>{{ day }}</h5>
                <p>{{ hour + " " + suffix }}</p>
            </div>
            <div class="info">
                <img :src="iconUrl" alt="Weather Icon">
                <p>{{ weather.weather[0].description | capitalize }}</p>
            </div>
            <div class="temperature">
                <h5>{{ weather.main.temp | round }}&deg; F</h5>
                <p>High: {{ weather.main.temp_max | round }}&deg;</p>
                <p>Low: {{ weather.main.temp_min | round }}&deg;</p>
            </div>
        </div>    
    </div>
</template>