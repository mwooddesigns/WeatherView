<template id="air-info">
    <div class="air-info">
        <p>Humidity: {{ air.humidity }}%</p>
        <p>Air Pressure: {{ air.pressure }} hPa</p>
        <p>Windspeed: {{ wind.speed | round }} Mph</p>
        <p>Cloudiness: {{ cloud.all }}%</p>
    </div>
</template>