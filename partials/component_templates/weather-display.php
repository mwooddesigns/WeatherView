<template id="weather-display">
    <div class="weather-display" v-if="display">
           <div class="row">
               <div class="col-12">
                   <h2>{{ componentName }}</h2>
               </div>
           </div>
            <div class="row break-sm">
                <div class="col-6">
                    <location-info :city_name="weather.name | capitalize" :country_code="weather.sys.country" :lat="weather.coord.lat | round" :lon="weather.coord.lon | round"></location-info>
                </div>
                <div class="col-6">
                    <temperature-info :current="weather.main.temp | round" :high="weather.main.temp_max | round" :low="weather.main.temp_min | round"></temperature-info>
                </div>
            </div>
            <div class="row reverse-break-sm">
               <div class="col-6">
                   <air-info :air="weather.main" :wind="weather.wind" :cloud="weather.clouds"></air-info>
               </div>
                <div class="col-6">
                    <additional-info :icon="weather.weather[0].icon" :description="weather.weather[0].description | capitalize" :sunrise="weather.sys.sunrise | formatTime" :sunset="weather.sys.sunset | formatTime"></additional-info>
                </div>
            </div>
        </div>
</template>
