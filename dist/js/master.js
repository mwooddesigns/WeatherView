"use strict";

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

Vue.filter("capitalize", function(value, method) {
    if (!value) return "";
    value = value.toString();
    var out = "";
    switch (method) {
      case "all":
        var strArr = value.split(" ");
        for (var i = 0; i < strArr; i++) {
            strArr[i] = strArr[i].capitalize();
        }
        out = strArr.join(" ");
        break;

      default:
        out = value.capitalize();
    }
    return out;
});

Vue.filter("round", function(value) {
    if (!value) return "";
    value = parseFloat(value);
    return Math.round(value);
});

Vue.filter("formatTime", function(value) {
    if (!value) return "";
    value = parseInt(value);
    var date = new Date(value * 1e3);
    var suffix = "AM";
    var hour = date.getHours();
    if (hour > 12) {
        hour = hour % 12;
        suffix = "PM";
    }
    var min = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var time = hour + ":" + min + " " + suffix;
    return time;
});

var AdditionalInfo = Vue.extend({
    props: [ "icon", "description", "sunrise", "sunset" ],
    computed: {
        iconUrl: function() {
            return "http://openweathermap.org/img/w/" + this.icon + ".png";
        }
    },
    template: "#additional-info"
});

Vue.component("additional-info", AdditionalInfo);

var AirInfo = Vue.extend({
    props: [ "air", "wind", "cloud" ],
    template: "#air-info"
});

Vue.component("air-info", AirInfo);

var ControlArea = Vue.extend({
    props: [ "current_city_name", "current_weather_mode" ],
    template: "#control-area",
    data: function() {
        return {
            localCurrentMode: "weather",
            modes: [ "weather", "forecast" ],
            localCityName: "Orlando"
        };
    },
    watch: {
        localCurrentMode: function() {
            this.$emit("change", this.localCurrentMode);
        },
        localCityName: function() {
            this.$emit("input", this.localCityName);
        }
    },
    methods: {
        handleCitySubmit: function() {
            this.$emit("enter", "City Submitted");
        }
    },
    created: function() {
        this.localCityName = this.current_city_name;
        this.localCurrentMode = this.current_weather_mode;
    }
});

Vue.component("control-area", ControlArea);

var ForecastCard = Vue.extend({
    props: [ "weather" ],
    data: function() {
        return {
            days: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
            date: new Date(this.weather.dt * 1e3)
        };
    },
    template: "#forecast-card",
    computed: {
        iconUrl: function() {
            return "http://openweathermap.org/img/w/" + this.weather.weather[0].icon + ".png";
        },
        day: function() {
            return this.days[this.date.getDay()];
        },
        hour: function() {
            var hours = this.date.getHours();
            return hours > 12 ? hours % 12 : hours;
        },
        suffix: function() {
            return this.date.getHours() > 12 ? "PM" : "AM";
        }
    }
});

Vue.component("forecast-card", ForecastCard);

var ForecastDisplay = Vue.extend({
    props: [ "forecast", "display" ],
    data: function() {
        return {
            componentName: "Forecast"
        };
    },
    template: "#forecast-display"
});

Vue.component("forecast-display", ForecastDisplay);

var LocationInfo = Vue.extend({
    props: [ "city_name", "country_code", "lat", "lon" ],
    template: "#location-info"
});

Vue.component("location-info", LocationInfo);

var TemperatureInfo = Vue.extend({
    props: [ "current", "high", "low" ],
    template: "#temperature-info"
});

Vue.component("temperature-info", TemperatureInfo);

var WeatherDisplay = Vue.extend({
    props: [ "weather", "display" ],
    data: function() {
        return {
            componentName: "Current Weather"
        };
    },
    template: "#weather-display"
});

Vue.component("weather-display", WeatherDisplay);

var weatherview = new Vue({
    el: "#weatherview",
    data: {
        appTitle: "WeatherView",
        cityName: "Orlando",
        modes: [ "weather", "forecast" ],
        currentMode: "weather",
        weatherData: null,
        showControls: false
    },
    computed: {
        isWeather: function() {
            return this.currentMode == "weather";
        },
        isForecast: function() {
            return this.currentMode == "forecast";
        }
    },
    mounted: function() {
        this.getWeatherData();
    },
    watch: {
        currentMode: function() {
            this.getWeatherData();
        }
    },
    methods: {
        getClientLocation: function() {
            var ctx = this;
            axios.get("./bin/get_location.php").then(function(response) {
                if (response.data.city != "" && response.data.region_code != "") {
                    ctx.cityName = response.data.city + "," + response.data.country_code;
                    this.getWeatherData();
                }
            });
        },
        getWeatherData: function() {
            var ctx = this;
            axios.get("./bin/get_weather.php?cityName=" + ctx.cityName + "&endpoint=" + ctx.currentMode).then(function(response) {
                ctx.weatherData = response.data;
            }).catch(function(error) {
                console.log(error);
            });
        },
        updateCityName: function(event) {
            var ctx = this;
            ctx.cityName = event;
        },
        updateMode: function(event) {
            var ctx = this;
            ctx.currentMode = event;
        },
        toggleControls: function() {
            var ctx = this;
            ctx.showControls = !ctx.showControls;
        }
    }
});