var LocationInfo = Vue.extend({
    props: ['city_name', 'country_code', 'lat', 'lon'],
    template: '#location-info'
});
Vue.component('location-info', LocationInfo);