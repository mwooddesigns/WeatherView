var TemperatureInfo = Vue.extend({
    props: ['current', 'high', 'low'],
    template: '#temperature-info'
});
Vue.component('temperature-info', TemperatureInfo);