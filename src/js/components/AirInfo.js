var AirInfo = Vue.extend({
    props: ['air', 'wind', 'cloud'],
    template: '#air-info'
});
Vue.component('air-info', AirInfo);