var ForecastDisplay = Vue.extend({
    props: ['forecast', 'display'],
    data: function() {
        return {
            componentName: "Forecast",
        }
    },
    template: '#forecast-display'
});
Vue.component('forecast-display', ForecastDisplay);