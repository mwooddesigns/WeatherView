<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>WeatherView</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="https://fonts.googleapis.com/css?family=Montserrat|Saira+Extra+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="node_modules/normalize.css/normalize.css">
    <link rel="stylesheet" href="dist/css/master.css">
</head>

<body>

    <div class="container">
        <div id="weatherview" class="fadeInFromTop">
            <div class="app-title">
                <h1>{{ appTitle }}</h1>
                <button class="toggle-controls-btn" @click="toggleControls"><i class="fa fa-bars" aria-hidden="true"></i></button>
            </div>
            <transition name="fade">
                <control-area v-if="showControls" :current_city_name="cityName" :current_weather_mode="currentMode" @enter="getWeatherData" @input="updateCityName" @change="updateMode"></control-area>
            </transition>
            <div class="display-panel">
                <weather-display :weather="weatherData" :display="isWeather"></weather-display>
                <forecast-display :forecast="weatherData" :display="isForecast"></forecast-display>
            </div>
        </div>
    </div>

    <?php include("partials/component_templates/master.php"); ?>

    <script type="text/javascript" src="node_modules/vue/dist/vue.min.js"></script>
    <script type="text/javascript" src="node_modules/axios/dist/axios.min.js"></script>
    <script type="text/javascript" src="dist/js/master.min.js"></script>

</body>

</html>
