var weatherview = new Vue({
    el: '#weatherview',
    data: {
        appTitle: "WeatherView",
        cityName: "Orlando",
        modes: ["weather", "forecast"],
        currentMode: "weather",
        weatherData: null,
        showControls: false
    },
    computed: {
        isWeather: function() {
            return this.currentMode == "weather";
        },
        isForecast: function() {
            return this.currentMode == "forecast";
        }
    },
    mounted: function () {
        // this.getClientLocation();
        this.getWeatherData()
    },
    watch: {
        'currentMode': function() {
            this.getWeatherData();
        }
    },
    methods: {
        getClientLocation: function () {
            var ctx = this;
            axios.get("./bin/get_location.php").then(function (response) {
                if (response.data.city != "" && response.data.region_code != "") {
                    ctx.cityName = response.data.city + "," + response.data.country_code;
                    this.getWeatherData()
                }
            });
        },
        getWeatherData: function () {
            var ctx = this;
            axios.get("./bin/get_weather.php?cityName=" + ctx.cityName + "&endpoint=" + ctx.currentMode).then(function (response) {
                ctx.weatherData = response.data;
            }).catch(function (error) {
                console.log(error);
            });
        },
        updateCityName: function(event) {
            var ctx = this;
            ctx.cityName = event;
        },
        updateMode: function(event) {
            var ctx = this;
            ctx.currentMode = event;
        },
        toggleControls: function() {
            var ctx = this;
            ctx.showControls = !ctx.showControls;
        }
    }
});
