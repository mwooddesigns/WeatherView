<template id="location-info">
    <div class="location-info">
        <h3>{{ city_name }}, {{ country_code }}</h3>
        <p>Lat: {{ lat }}&deg; | Lon: {{ lon }}&deg;</p>
    </div>
</template>
