var ForecastCard = Vue.extend({
    props: ['weather'],
    data: function() {
        return {
            days: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            date: new Date(this.weather.dt * 1000),
        }
    },
    template: '#forecast-card',
    computed: {
        iconUrl: function () {
            return 'http://openweathermap.org/img/w/' + this.weather.weather[0].icon + '.png';
        },
        day: function() {
            return this.days[this.date.getDay()];
        },
        hour: function() {
            var hours = this.date.getHours();
            return hours > 12 ? hours % 12 : hours;
        },
        suffix: function() {
            return this.date.getHours() > 12 ? "PM" : "AM";
        }
    },
});
Vue.component('forecast-card', ForecastCard);