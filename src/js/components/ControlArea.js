var ControlArea = Vue.extend({
    props: ['current_city_name', 'current_weather_mode'],
    template: '#control-area',
    data: function () {
        return {
            localCurrentMode: 'weather',
            modes: ['weather', 'forecast'],
            localCityName: 'Orlando'
        };
    },
    watch: {
        'localCurrentMode': function () {
            this.$emit('change', this.localCurrentMode);
        },
        'localCityName': function () {
            this.$emit('input', this.localCityName);
        }
    },
    methods: {
        handleCitySubmit: function() {
            this.$emit('enter', 'City Submitted');
        }
    },
    created: function () {
        this.localCityName = this.current_city_name;
        this.localCurrentMode = this.current_weather_mode;
    }
});
Vue.component('control-area', ControlArea);
