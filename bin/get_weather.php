<?php
include("functions.php");

$API_KEY = "e030fb5cef4eb7ae8b261d5fb4933b10";

$city = isset($_GET["cityName"]) ? $_GET["cityName"] : "Orlando, US";
$units = isset($_GET["units"]) ? $_GET["units"] : "imperial";
$lang = isset($_GET["lang"]) ? $_GET["lang"] : "en";
$endpoint = isset($_GET["endpoint"]) ? $_GET["endpoint"] : "weather";

$url = "http://api.openweathermap.org/data/2.5/$endpoint?q=$city&units=$units&lang=$lang&APPID=$API_KEY";

$data = call_api("GET", $url);

die($data);