var WeatherDisplay = Vue.extend({
    props: ['weather', 'display'],
    data: function() {
        return {
            componentName: "Current Weather",
        }
    },
    template: '#weather-display'
});
Vue.component('weather-display', WeatherDisplay);