<template id="additional-info">
    <div class="additional-info"><img :src="iconUrl" :alt="description" />
        <p>{{ description }}</p>
        <p>Sunrise: {{ sunrise }}</p>
        <p>Sunset: {{ sunset }}</p>
    </div>
</template>
