var AdditionalInfo = Vue.extend({
    props: ['icon', 'description', 'sunrise', 'sunset'],
    computed: {
        iconUrl: function () {
            return 'http://openweathermap.org/img/w/' + this.icon + '.png';
        }
    },
    template: '#additional-info'
});
Vue.component('additional-info', AdditionalInfo);