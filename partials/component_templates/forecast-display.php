<template id="forecast-display">
    <div class="forecast-display" v-if="display">
        <div class="row">
           <div class="col-12">
               <h2>{{ componentName }}</h2>
               <h5>{{ forecast.city.name + ", " + forecast.city.country }}</h5>
               <p>Lat: {{ forecast.city.coord.lat | round }}&deg; |  Lon: {{ forecast.city.coord.lon | round }}&deg;</p>
           </div>
           <div class="row break-sm">
               <forecast-card v-for="item in forecast.list" :weather="item"></forecast-card>
           </div>
       </div>
    </div>
</template>
