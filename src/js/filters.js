Vue.filter('capitalize', function (value, method) {
    if (!value) return "";
    value = value.toString();
    var out = "";
    switch (method) {
        case "all":
            var strArr = value.split(" ");
            for (var i = 0; i < strArr; i++) {
                strArr[i] = strArr[i].capitalize();
            }
            out = strArr.join(" ");
            break;
        default:
            out = value.capitalize();
    }
    return out;
});

Vue.filter('round', function (value) {
    if (!value) return '';
    value = parseFloat(value);
    return Math.round(value);
});

Vue.filter('formatTime', function (value) {
    if (!value) return '';
    value = parseInt(value);
    var date = new Date(value * 1000);
    var suffix = 'AM';
    var hour = date.getHours();
    if (hour > 12) {
        hour = hour % 12;
        suffix = "PM";
    }
    var min = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
    var time = hour + ':' + min + ' ' + suffix;
    return time;
});
