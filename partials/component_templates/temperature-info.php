<template id="temperature-info">
    <div class="temperature-info">
        <h3>{{ current }}&deg; F</h3>
        <p>High: {{ high }}&deg; F | Low: {{ low }}&deg; F</p>
    </div>  
</template>